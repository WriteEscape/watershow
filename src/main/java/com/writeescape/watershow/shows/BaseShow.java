package com.writeescape.watershow.shows;

import com.writeescape.watershow.Watershow;
import com.writeescape.watershow.fountain.Shooter;
import com.writeescape.watershow.managers.BlockCollector;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

import static com.writeescape.watershow.Watershow.log;


public abstract class BaseShow {
    public static final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    String name;
    Location l = null;
    Double r;
    boolean isSpawned;
    boolean isFinished;
    private boolean playingSong;
    private int innerR;
    private final static int LOOP_WAIT = 1000;
    private static final HashMap<Integer,ArrayList<Integer>> octave = new HashMap<>();
    private static final HashMap<String,String> opposites = new HashMap<>();
    private static final ArrayList<Location> middles = new ArrayList<Location>();
    private static final ArrayList<ArmorStand> st = new ArrayList<ArmorStand>();
    private long f = 0;
    BaseShow(String n){
        this.name = n;
        isSpawned = false;
        playingSong = false;

    }
    public void setL(Location s){
        this.l = new Location(s.getWorld(),s.getBlockX(),s.getBlockY(),s.getBlockZ());

    }
    public void setL(String World, double x, double y, double z ) {
        this.l = new Location(Watershow.getInstance().getServer().getWorld(World),x,y,z);
    }

    public String getName() {
        return name;
    }

    public void SpawnShow(){
        if(l != null && r != null){
            isSpawned = true;
            getFountain();

        } else {
            isSpawned = false;
        }
    }

    public void setR(Double r) {
        this.r = r;
    }
    public Double getShowRadius(){
        return r;
    }
    public boolean isSpawned() {
        return isSpawned;
    }
    public Location getLocation(){
        return l;
    }

    public boolean isFinished() {
        return isFinished;
    }


    public void play(long d){

        if(!isSpawned()){
            SpawnShow();
        } else {


        }
    }

    public void setPlayingSong(boolean yes){
        playingSong = yes;
    }



    public void getFountain(){
        if(l != null){
            int o = 0;
            Location middle = l.clone();
            for (int i = 0; i < 360; i += 360 / 8) {
                double angle = (i * Math.PI / 180);

                Location newl = getLocationAroundCircle(middle,r,angle);
                synchronized (middles){
                    middles.add(newl);
                }
                octaveFountains(newl, getRadius(36),o);
                o++;
            }
        }

    }

    private double getRadius(int r){
        double ir = 0.125;
        if(r != 0) {
            for (int i = 0; i < r; i++) {
                ir += 0.125;

            }
        } else {
            return 0.125;
        }
        return ir;
    }



    public ArrayList<Integer> getOctaveStands(int o){
       return octave.get(o);
    }

    private void octaveFountains(Location middle, double size,int o){
        int n = 0;
        FileConfiguration c = getConfig();
        ArrayList<Integer> tmp = new ArrayList<>();
        for (int i = 0; i < 360; i += 360 / 12) {
            double angle = (i * Math.PI / 180);

            Location loc = getLocationAroundCircle(middle,size,angle);
            synchronized (st) {
                ArmorStand at = (ArmorStand) Objects.requireNonNull(loc.getWorld()).spawnEntity(loc, EntityType.ARMOR_STAND);
                at.setArms(false);
                at.setSmall(true);
                at.setGravity(true);
                at.setCustomNameVisible(true);
                at.setCustomName(n+"-"+o);
                st.add(at);
                tmp.add(st.size()-1);
            }
                synchronized (opposites){
                String in = n+"-"+o;
                    if(!c.contains("show."+name+".opposites."+in)) {
                        ConfigurationSection cs = c.createSection("show." + name + ".opposites." + in);
                        cs.set("note", -1);
                        cs.set("octave", -1);
                    }

                }
            n++;
        }
        try {
            c.save(new File(Watershow.getInstance().getDataFolder(), "shows.yml"));
        } catch (IOException ignored){

        }
        synchronized (octave){
            octave.put(o,tmp);
        }
    }



    public Location getLocationAroundCircle(Location center, double radius, double angleInRadian) {
        double x = center.getX() + radius * Math.cos(angleInRadian);
        double z = center.getZ() + radius * Math.sin(angleInRadian);
        double y = center.getY();

        Location loc = new Location(center.getWorld(), x, y, z);
        Vector difference = center.toVector().clone().subtract(loc.toVector()); // this sets the returned location's direction toward the center of the circle
        loc.setDirection(difference);

        return loc;
    }

    public ArmorStand getNoteLocation(int n, int o){
        ArmorStand a;
        try {
            //log(Level.INFO,"Note:"+n+" Octave:"+o);
            ArrayList<Integer> s = getOctaveStands(o);
            Integer note = s.get(n);
            a = st.get(note);
        } catch (NullPointerException e){
            a = st.get(0);
        }
        return a;
    }

    public void remove(){
        if(l.getWorld() != null) {
            List<Entity> e = l.getWorld().getEntities();
            for (Entity entity : (Entity[]) e.toArray()) {
                if (entity instanceof ArmorStand) {
                    ArmorStand as = (ArmorStand) entity;

                    if (as.getCustomName().equalsIgnoreCase("WaterShow-" + name)) {
                        as.remove();
                    }
                } else if (entity instanceof FallingBlock) {
                    FallingBlock fb = (FallingBlock) entity;
                    fb.remove();
                }
            }
        }
    }


    public Entity getEntity(int n, int o, float offset, BlockData b){
        Entity e = null;
        if(n == 12){
            n = 0;
            o = o + 1;
        }
        if(o == 8){
            o = 0;
        }
        ArmorStand as = getNoteLocation(n,o);
        Location nl = as.getLocation();
        Location ol = getOpposite(n,o);
        String instrument = Watershow.getShowUtil().isInstrument(b.getMaterial());
        ArrayList<Integer> os = getOctaveStands(o);
        Integer note = os.get(n);
        Vector vec = new Vector(0, 0, 0);
        Vector newVec = new Vector(ol.getX()-nl.getX(), (ol.getY()+(1*offset)) - nl.getY(), ol.getZ() -nl.getZ());
        if(instrument.equalsIgnoreCase("p")){
            FallingBlock fb = Objects.requireNonNull(nl.getWorld()).spawnFallingBlock(nl, b);
            fb.setDropItem(false);
            fb.setHurtEntities(false);


            fb.setVelocity(vec.add(newVec).normalize());
            e = fb;
        } else if(instrument.equalsIgnoreCase("d")){

            Snowball s = st.get(note).launchProjectile(Snowball.class,vec.add(newVec).normalize());
            s.setBounce(true);
            ItemStack is = new ItemStack(b.getMaterial());
            s.setItem(is);

            e = s;
        }
        return e;


    }

    public ArrayList<Location> getMiddles(){
        return middles;
    }


    public FileConfiguration getConfig(){
        FileConfiguration fc = Watershow.getInstance().getConfig();
        try {
            fc.load(new File(Watershow.getInstance().getDataFolder(), "shows.yml"));
        } catch (IOException | InvalidConfigurationException e){

        }
        return  fc;
    }
    public FileConfiguration setConfig(){
        FileConfiguration fc = getConfig();
        fc.createSection("show."+name);
        ConfigurationSection cfgs = fc.getConfigurationSection("show."+name);
        ConfigurationSection location = cfgs.createSection("location");
        Location s = getLocation();
        location.set("world",s.getWorld().getName());
        location.set("x",s.getBlockX());
        location.set("y",s.getBlockY());
        location.set("z",s.getBlockZ());
        cfgs.set("radius",r);
        return fc;

    }

    public Location getOpposite(int n, int o){
        String in = n+"-"+o;
        FileConfiguration cfg = getConfig();
        int on = -1;
        int oo = -1;
        if(cfg.contains("show."+name+".opposites."+in)){

            on = cfg.getInt("show."+name+".opposites."+in+"note");
            oo = cfg.getInt("show."+name+".opposites."+in+"octave");
        } 
        Location lc;
        if(on == -1 && oo == -1){
            lc = l.clone().add(0,100,0);
            
        } else {
            ArmorStand stand = getNoteLocation(on,oo);
            lc = stand.getLocation().add(0,10,0);
        }
        
        return lc;
    }

    public Location getMiddle(int o){
        ArrayList<Location> m = getMiddles();
        return m.get(o);
    }

    public Location lookAt(Location npcLoc, int oct) {
        Location point = getMiddle(oct);

        double xDiff = point.getX() - npcLoc.getX();
        double yDiff = point.getY() - npcLoc.getY();
        double zDiff = point.getZ() - npcLoc.getZ();

        double DistanceXZ = Math.sqrt(xDiff * xDiff + zDiff * zDiff);
        double DistanceY = Math.sqrt(DistanceXZ * DistanceXZ + yDiff * yDiff);
        double newYaw = Math.acos(xDiff / DistanceXZ) * 180 / Math.PI;
        double newPitch = Math.acos(yDiff / DistanceY) * 180 / Math.PI - 90;
        if (zDiff < 0.0)
            newYaw = newYaw + Math.abs(180 - newYaw) * 2;
        newYaw = (newYaw - 90);

        float yaw = (float) newYaw;
        float pitch = (float) newPitch;

        Location n = npcLoc.clone();
        n.setPitch(pitch);
        n.setYaw(yaw);
        return n;
    }

}
