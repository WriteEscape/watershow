package com.writeescape.watershow;

import com.writeescape.watershow.commands.ShowCommand;
import com.writeescape.watershow.managers.BlockCollector;
import com.writeescape.watershow.runnable.FountainShow;
import com.writeescape.watershow.shows.BaseShow;
import com.writeescape.watershow.shows.Shows;
import com.writeescape.watershow.utils.LoadShows;
import com.writeescape.watershow.utils.ShowUtil;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.PluginCommand;
import com.writeescape.watershow.commands.ReloadCommand;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Watershow extends JavaPlugin {

    private static ShowUtil showUtil;
    private static Watershow instance;
    private static final Logger s_log = Logger.getLogger("Minecraft.WaterShow");
    private static String s_prefix = null;
    private static final String s_logFormat = "%s %s";
    private static FountainShow m_musicPlayer;
    private static String m_version;


    @Override
    public void onEnable() {
        Server server = getServer();
        PluginDescriptionFile desc = getDescription();

        s_prefix = String.format("[%s]", desc.getName());
        

        m_version = desc.getVersion();
        m_musicPlayer = new FountainShow(this, server.getScheduler());

        ShowCommand showCommand = new ShowCommand(this,m_musicPlayer);
        PluginCommand commandShow = getCommand("show");
        commandShow.setExecutor(showCommand);
        getServer().getPluginManager().registerEvents(new ShowListener(),this);
        // Plugin startup logic
        instance = this;
        showUtil = new ShowUtil();


        if(!getDataFolder().exists()){
            getDataFolder().mkdir();

        }

        loadShows();
    }

    @Override
    public void onDisable() {
        m_musicPlayer.stop();
    }
    public static Watershow getInstance() {
        return instance;
    }
    public static ShowUtil getShowUtil(){
        return showUtil;
    }



    public static void log(Level lvl, String msg) {
        if (s_log == null || msg == null || s_prefix == null) {
            return;
        }

        s_log.log(lvl, String.format(s_logFormat, s_prefix, msg));
    }
    public static void say(Player player, String msg) {
        if (player == null) {
            log(Level.INFO, msg);
        } else {
            player.sendRawMessage(msg);
        }
    }
    public String getVersion() {
        return m_version;
    }

    public static ArrayList<BaseShow> getShows(){
        return (ArrayList<BaseShow>) m_musicPlayer.getPlayingShows();
    }
    public boolean getShowExsists(String name){
        boolean c = false;
        FileConfiguration f = getConfig();
        try {
            f.load(new File(getDataFolder(), "shows.yml"));
            c = f.contains("show."+name);

        } catch (InvalidConfigurationException | IOException e) {
            e.printStackTrace();
        }
        return c;
    }
    public void loadShows(){
        FileConfiguration fc = getConfig();
        try {
            fc.load(  new File(getDataFolder(), "shows.yml"));
        } catch (IOException | InvalidConfigurationException ignored){

        }
        List<String> s = fc.getStringList("show");
        for(String showname : s){
            String path = "show."+showname+".";
            Shows show = new Shows(showname);
            show.setL(fc.getString(path+"location.world"),fc.getInt(path+"location.x"),fc.getInt(path+"location.y"),fc.getInt(path+"location.z"));
            show.setR(fc.getDouble(path+"radius"));
            m_musicPlayer.playShow(show);
        }

    }
}
