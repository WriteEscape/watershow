package com.writeescape.watershow;

import com.writeescape.watershow.events.RotateShow;
import com.writeescape.watershow.events.ShowEvents;
import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityEnterBlockEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;


import java.util.ArrayList;

public class ShowListener implements Listener {
    @EventHandler
    public void onFallingBlockLand(final EntityChangeBlockEvent e) {
        if (e.getEntity() instanceof FallingBlock) {

            e.setCancelled(true);
        }
    }
    @EventHandler
    public void onFallingBlockHitsWater(final ProjectileHitEvent e){
        if(e.getEntity() instanceof Snowball && e.getHitEntity() instanceof ArmorStand) {
            Snowball fb = (Snowball) e.getEntity();
            Location l = Watershow.getShows().get(0).getLocation().add(0,20D,0);
            Location cur = fb.getLocation();
            Vector difference = l.toVector().clone().subtract(cur.toVector());
            fb.setVelocity(difference);
        }
    }

    @EventHandler
    public void ShowHandler(final ShowEvents e){
        BaseShow show = e.getShow();
        Entity entity = e.getEntity();
    }

    @EventHandler
    public void playerLogout(final PlayerQuitEvent e){

    }
    
}
