package com.writeescape.watershow.commands;

import com.writeescape.watershow.Watershow;
import com.writeescape.watershow.midiparser.MidiParser;
import com.writeescape.watershow.midiparser.NoteFrame;
import com.writeescape.watershow.midiparser.NoteTrack;
import com.writeescape.watershow.runnable.FountainShow;
import com.writeescape.watershow.shows.BaseShow;
import com.writeescape.watershow.shows.Shows;
import com.writeescape.watershow.track.LocationTrack;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

import static com.writeescape.watershow.Watershow.log;
import static com.writeescape.watershow.Watershow.say;

public class ShowCommand extends BaseCommand {
    private final Watershow m_plugin;
    private final FountainShow player;
    private LocationTrack m_currentTrack;
    public ShowCommand(Watershow plugin, FountainShow player){
        this.m_plugin = plugin;
        this.player = player;
        m_currentTrack = null;
    }
    public boolean onCommand(CommandSender cs, Command cmnd, String name, String[] args) {
        Player player = (cs instanceof Player) ? (Player) cs : null;

        if(args.length > 0) {

            if (args[0].equalsIgnoreCase("new")) {
                if (!args[1].isEmpty() && !m_plugin.getShowExsists(args[0])) {

                    Shows s = new Shows(args[1]);

                    if(player != null) {
                        s.setL(player.getLocation());
                    } else {

                        s.setL(args[2],Integer.parseInt(args[3]),Integer.parseInt(args[4]),Integer.parseInt(args[5]));
                    }
                    s.setR(12D);

                    this.player.playShow(s);

                    int sc = this.player.getPlayingShows().size()-1;
                    cs.sendMessage("Added Show:" + sc);
                    try {
                        s.setConfig().save(new File(m_plugin.getDataFolder(),"shows.yml"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    cs.sendMessage("Can't add a Show without name and where you would like it to be put!");

                }
            } else if (args[0].equalsIgnoreCase("remove")) {
                if (!args[1].isEmpty()) {
                    for (Shows s : (Shows[]) Watershow.getShows().toArray()) {
                        if (s.getName().equalsIgnoreCase(args[1])) {
                            this.player.removeShow(s);
                        }
                    }
                } else {
                    cs.sendMessage("Can't remove a show without a name");
                }
            } else if(args[0].equalsIgnoreCase("play")){
                this.player.removeTrack(m_currentTrack);

                String fileName = args[1];
                if (fileName == null) {
                    return true;
                }

                boolean loop = args[2].equalsIgnoreCase("true");


                File f = new File(m_plugin.getDataFolder(), fileName);

                NoteTrack noteTrack = MidiParser.loadFile(f);
                if (noteTrack == null) {
                    say(player, "Error loading " + fileName + " midi track");
                    log(Level.WARNING, "Error loading " + fileName + " midi track");
                    return true;
                } else if (noteTrack.isError()) {
                    say(player, "Error loading " + fileName + " midi track: " + noteTrack.getMessage());
                    log(Level.WARNING, "Error loading " + fileName + " midi track: " + noteTrack.getMessage());
                    return true;
                }

                final NoteFrame[] notes = noteTrack.getNotes();


                BaseShow show = this.player.getPlayingShows().get(Integer.parseInt(args[3]));

                m_currentTrack = new LocationTrack(notes,loop,show);

                this.player.playTrack(m_currentTrack);
            }
            return true;
        }
        return false;
    }

}
