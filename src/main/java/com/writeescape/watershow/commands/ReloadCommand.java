package com.writeescape.watershow.commands;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.writeescape.watershow.configuration.ConfigProvider;
import com.writeescape.watershow.Watershow;
import static com.writeescape.watershow.Watershow.log;
import com.writeescape.watershow.instruments.MapFileParser;

/**
 * Reload configuration command
 * @author SBPrime
 */
public class ReloadCommand extends BaseCommand {

    private final Watershow m_pluginMain;

    public ReloadCommand(Watershow pluginMain) {
        m_pluginMain = pluginMain;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String name, String[] args) {
        if (args != null && args.length > 0) {
            return false;
        }

        Player player = (cs instanceof Player) ? (Player) cs : null;

        m_pluginMain.reloadConfig();
        ReloadConfig(player);
        return true;
    }

    public boolean ReloadConfig(Player player) {
        if (!ConfigProvider.load(m_pluginMain)) {
            Watershow.say(player, "Error loading config");
            return false;
        }
        /*
        if (ConfigProvider.getCheckUpdate()) {
           log(Level.INFO, VersionChecker.CheckVersion(m_pluginMain.getVersion()));
        }*/
        if (!ConfigProvider.isConfigUpdated()) {
            log(Level.INFO, "Please update your config file!");
        }

        if (!ReloadInstrumentMap(player)) {
            return false;
        }
        Watershow.say(player, "Config loaded");
        return true;
    }

    private boolean ReloadInstrumentMap(Player player) {
        String instrumentName = ConfigProvider.getInstrumentMapFile();
        String drumName = ConfigProvider.getDrumMapFile();
        File instrumentFile = new File(ConfigProvider.getPluginFolder(), instrumentName);
        File drumFile = new File(ConfigProvider.getPluginFolder(), drumName);

        if (MapFileParser.loadMap(instrumentFile)) {
            Watershow.say(player, "Instrument map file loaded.");
        } else {
            Watershow.say(player, "Error loading instrument map file");
            if (!MapFileParser.loadDefaultMap()) {
                Watershow.say(player, "Error loading default instrument map.");
                return false;
            } else {
                Watershow.say(player, "Loaded default instrument map.");
            }
        }

        if (MapFileParser.loadDrumMap(drumFile)) {
            Watershow.say(player, "Drum map file loaded.");
        } else {
            Watershow.say(player, "Error loading drum map file");
            if (!MapFileParser.loadDefaultDrumMap()) {
                Watershow.say(player, "Error loading default drum map.");
                return false;
            } else {
                Watershow.say(player, "Loaded default drum map.");
            }
        }
        return true;
    }
}
