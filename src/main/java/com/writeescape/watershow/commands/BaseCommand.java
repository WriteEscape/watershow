package com.writeescape.watershow.commands;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 *
 * @author prime
 */
public abstract class BaseCommand implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String name, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender cs, Command cmnd, String name, String[] args) {
        return new ArrayList<String>();
    }
}
