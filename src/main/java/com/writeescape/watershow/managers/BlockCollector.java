package com.writeescape.watershow.managers;

import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;

import java.util.ArrayList;

public class BlockCollector {
    private final ArrayList<Entity> blocks;

    public BlockCollector() {
        this.blocks = new ArrayList<Entity>();
    }

    public ArrayList<Entity> getBlocks() {
        return this.blocks;
    }

    public void removeBlock(final Entity b) {
        if (this.getBlocks().contains(b)) {
            this.blocks.remove(b);
        }
    }
    public void addBlock(final Entity b){
        if(!this.getBlocks().contains(b)){
            this.blocks.add(b);
        }
    }
}
