package com.writeescape.watershow.events;

import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BaseEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private BaseShow show;

    public BaseEvent(BaseShow s) {
        show = s;
    }

    public BaseShow getShow() {
        return show;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
