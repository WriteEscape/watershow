package com.writeescape.watershow.events;

import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RotateShow extends Event {

    private static final HandlerList handlers = new HandlerList();
    private BaseShow show;
    private boolean cancelled;
    int r;
    public RotateShow(BaseShow s,int rad) {
        show = s;
        r = rad;
    }

    public BaseShow getShow() {
        return show;
    }

    public double getRadPerTick(){
        return (r * Math.PI / 180);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
}
