package com.writeescape.watershow.events;

import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ShowEvents extends Event {
    private static final HandlerList handlers = new HandlerList();
    private BaseShow show;
    private boolean cancelled;
    private Entity e;
    int r;
    public ShowEvents(BaseShow s) {
        show = s;

    }

    public void setEntity(Entity entity){
        e = entity;
    }

    public void getDirection(){

    }

    public Entity getEntity(){
        return e;
    }

    public BaseShow getShow() {
        return show;
    }

    public double getRadPerTick(){
        return (r * Math.PI / 180);
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
}
