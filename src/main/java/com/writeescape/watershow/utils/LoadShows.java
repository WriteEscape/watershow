package com.writeescape.watershow.utils;

import com.writeescape.watershow.Watershow;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import static com.writeescape.watershow.Watershow.log;
import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

public class LoadShows {
    Watershow plugin;
    public LoadShows(Watershow s){
        plugin = s;
        FileConfiguration f = s.getConfig();
        File fs = new File(s.getDataFolder(),"shows.yml");
        if(fs.exists()) {
            try{
                f.load(fs);
                loadShow(f);
            } catch (InvalidConfigurationException |  IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadShow(FileConfiguration f){
        ArrayList<String> s = (ArrayList<String>) f.getStringList("show");
        for(String showname : (String[]) s.toArray()){
            log(Level.INFO,showname);
        }
    }

}
