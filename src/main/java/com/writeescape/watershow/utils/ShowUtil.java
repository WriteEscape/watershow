package com.writeescape.watershow.utils;
import org.bukkit.Color;
import org.bukkit.DyeColor;

import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Entity;


import java.util.ArrayList;


public class ShowUtil {
    private final ArrayList<Entity> entities = new ArrayList<>();
    private static ArrayList<Material> instrumentMap = new ArrayList<>();
    public ShowUtil(){
        instrumentMap = new ArrayList<>();
    }
    public BlockData getPiano(int i){

        DyeColor[] d = DyeColor.values();

        return Material.getMaterial(d[i].toString()+"_STAINED_GLASS").createBlockData();
    }
    public BlockData getDrums(int i){
        DyeColor[] d = DyeColor.values();

        return Material.getMaterial(d[i].toString()+"_STAINED_GLASS_PANE").createBlockData();

    }

    public Color getColor(int i,int t){
        DyeColor[] b = DyeColor.values();
        if(t == 0){
            return  b[i].getColor();
        } else {
            return b[i].getFireworkColor();
        }
    }
    public String isInstrument(Material m){
        String b = "";
        for(int i = 0; i<=15; i++){
            if(m == getPiano(i).getMaterial()){
                b = "p";
            }
            if(m == getDrums(i).getMaterial()){
                b = "d";
            }
        }
        return b;
    }

    public void addEntity(Entity f){
        synchronized (entities){
            entities.add(f);
        }
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }
}

