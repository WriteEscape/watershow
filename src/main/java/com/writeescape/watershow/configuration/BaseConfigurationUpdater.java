package com.writeescape.watershow.configuration;


import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author SBPrime
 */
public abstract class BaseConfigurationUpdater implements IConfigurationUpdater {
    protected int getAndRemoveInt(ConfigurationSection section, String key, int defaultValue) {
        if (section == null) {
            return defaultValue;
        }

        if (!section.contains(key)) {
            return defaultValue;
        }

        int result = section.getInt(key, defaultValue);
        section.set(key, null);

        return result;
    }


    protected boolean getAndRemoveBoolean(ConfigurationSection section, String key, boolean defaultValue) {
        if (section == null) {
            return defaultValue;
        }

        if (!section.contains(key)) {
            return defaultValue;
        }

        boolean result = section.getBoolean(key, defaultValue);
        section.set(key, null);

        return result;
    }


    protected ConfigurationSection getOrCreate(ConfigurationSection section, String name) {
        if (!section.contains(name)) {
            section.createSection(name);
        }

        return section.getConfigurationSection(name);
    }


    protected void setIfNone(ConfigurationSection section, String key, Object value) {
        section.set(key, section.get(key, value));
    }
}
