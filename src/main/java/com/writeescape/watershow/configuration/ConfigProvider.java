package com.writeescape.watershow.configuration;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import org.bukkit.SoundCategory;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationOptions;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.plugin.java.JavaPlugin;
import static com.writeescape.watershow.Watershow.log;

/**
 * This class contains configuration
 *
 * @author SBPrime
 */
public class ConfigProvider {

    /**
     * Number of ticks in one second
     */
    public static final int TICKS_PER_SECOND = 20;

    private static boolean m_checkUpdate = false;

    private static boolean m_isConfigUpdate = false;

    private static String m_configVersion;

    private static File m_pluginFolder;

    private static String m_instrumentMap;

    private static String m_drumMap;

    private static SoundCategory m_soundCategory;

    /**
     * Plugin root folder
     *
     * @return The root folder of this plugin
     */
    public static File getPluginFolder() {
        return m_pluginFolder;
    }

    /**
     * Get the config version
     *
     * @return Current config version
     */
    public static String getConfigVersion() {
        return m_configVersion;
    }

    /**
     * Is update checking enabled
     *
     * @return true if enabled
     */
    public static boolean getCheckUpdate() {
        return m_checkUpdate;
    }

    /**
     * Is the configuration up to date
     *
     * @return Whether the configuration is up to date or not
     */
    public static boolean isConfigUpdated() {
        return m_isConfigUpdate;
    }

    public static String getInstrumentMapFile() {
        return m_instrumentMap;
    }

    public static String getDrumMapFile() {
        return m_drumMap;
    }

    public static SoundCategory getSoundCategory() {
        return m_soundCategory;
    }

    /**
     * Load configuration
     *
     * @param plugin parent plugin
     * @return true if config loaded
     */
    public static boolean load(JavaPlugin plugin) {
        if (plugin == null) {
            return false;
        }

        plugin.saveDefaultConfig();
        m_pluginFolder = plugin.getDataFolder();

        Configuration config = plugin.getConfig();
        ConfigurationSection mainSection = config.getConfigurationSection("WaterShow");
        if (mainSection == null) {
            return false;
        }

        int configVersion = mainSection.getInt("version", 0);
        if (configVersion < ConfigurationUpdater.CONFIG_VERSION) {
            if (ConfigurationUpdater.updateConfig(config, configVersion)) {
                SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");
                File oldConfig = new File(m_pluginFolder, "config.yml");
                File newConfig = new File(m_pluginFolder, String.format("config.v%1$s", formater.format(new Date())));

                oldConfig.renameTo(newConfig);

                ConfigurationOptions options = config.options();
                if (options instanceof FileConfigurationOptions) {
                    FileConfigurationOptions fOptions = (FileConfigurationOptions) options;
                    fOptions.header(null);
                    fOptions.copyHeader(true);
                }

                plugin.saveConfig();

                int newVersion = mainSection.getInt("version", 0);
                log(Level.INFO, String.format("Configuration updated from %1$s to %2$s.", configVersion, newVersion));
                if (newVersion != ConfigurationUpdater.CONFIG_VERSION) {
                    log(Level.INFO, String.format("Unable to update config to the required version (%1$s).", ConfigurationUpdater.CONFIG_VERSION));
                }
            } else {
                log(Level.INFO, String.format("Unable to update config to the required version (%1$s).", ConfigurationUpdater.CONFIG_VERSION));
            }
        }

        m_configVersion = mainSection.getString("version", "?");

        m_checkUpdate = mainSection.getBoolean("checkVersion", true);
        m_isConfigUpdate = mainSection.getInt("version", 0) == ConfigurationUpdater.CONFIG_VERSION;
        m_instrumentMap = mainSection.getString("map", "");
        m_drumMap = mainSection.getString("drum", "");

        m_soundCategory = parseSoundCategory(mainSection.getString("soundCategory", "music"));

        return true;
    }

    private static SoundCategory parseSoundCategory(String categoryName) {
        if (categoryName != null) {
            categoryName = categoryName.trim();
            for (SoundCategory c : SoundCategory.values()) {
                if (categoryName.equalsIgnoreCase(c.name())) {
                    return c;
                }
            }
        }

        log(Level.WARNING, "Specified SoundCategory not found! Using " + SoundCategory.MUSIC.name());
        return SoundCategory.MUSIC;
    }
}
