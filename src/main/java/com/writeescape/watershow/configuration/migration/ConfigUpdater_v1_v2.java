package com.writeescape.watershow.configuration.migration;

import com.writeescape.watershow.configuration.BaseConfigurationUpdater;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;

import java.util.logging.Level;

import static com.writeescape.watershow.Watershow.log;


/**
 *
 * @author SBPrime
 */
public class ConfigUpdater_v1_v2 extends BaseConfigurationUpdater {
    @Override
    public int updateConfig(Configuration config) {
        log(Level.INFO, "Updating configuration v1 --> v2");

        ConfigurationSection mainSection = config.getConfigurationSection("awe");
        if (mainSection == null) {
            return -1;
        }


        setIfNone(mainSection, "soundCategory", "music");
        mainSection.set("version", 2);

        return 2;
    }
}
