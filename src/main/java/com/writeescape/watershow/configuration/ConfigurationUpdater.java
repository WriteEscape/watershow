package com.writeescape.watershow.configuration;

import java.util.HashMap;
import java.util.Map;

import com.writeescape.watershow.configuration.migration.ConfigUpdater_v1_v2;
import org.bukkit.configuration.Configuration;

/**
 * The automatic configuration updater.
 * Updates configuration files in sequence
 * @author SBPrime
 */
public class ConfigurationUpdater {
    private final static Map<Integer, IConfigurationUpdater> s_configurationUpdaters;


    /**
     * The config file version
     */
    public static final int CONFIG_VERSION = 2;


    static {
        s_configurationUpdaters = new HashMap<Integer, IConfigurationUpdater>();
        s_configurationUpdaters.put(1, new ConfigUpdater_v1_v2());
    }

    public static boolean updateConfig(Configuration config, int version) {
        int oldVersion = version;
        int newVersion = version;
        while (s_configurationUpdaters.containsKey(oldVersion)) {
            IConfigurationUpdater updater = s_configurationUpdaters.get(oldVersion);

            newVersion = updater.updateConfig(config);

            if (newVersion < 0) {
                return false;
            }

            oldVersion = newVersion;
        }

        return newVersion != version;
    }
}
