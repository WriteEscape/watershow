package com.writeescape.watershow.configuration;

import org.bukkit.configuration.Configuration;
public interface IConfigurationUpdater {
    int updateConfig(Configuration config);
}
