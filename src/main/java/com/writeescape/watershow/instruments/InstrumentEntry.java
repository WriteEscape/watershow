package com.writeescape.watershow.instruments;

import org.bukkit.Material;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Entity;

public class InstrumentEntry {
    /**
     * Instrument path
     */
    private final BlockData m_patch;


    /**
     * The volume scale
     */
    private final float m_volumeScale;


    /**
     * Sound patch
     * @return The patch of this instrument
     */
    public BlockData getPatch() {
        return m_patch;
    }

    /**
     * Get the volume scale
     * @return The volume scale for this instrument
     */
    public float getVolumeScale() {
        return m_volumeScale;
    }

    public InstrumentEntry(String patch, float volumeScale){
        m_patch = Material.getMaterial(patch).createBlockData();
        m_volumeScale = volumeScale;
    }

    public InstrumentEntry(BlockData patch, float volumeScale) {
        m_patch = patch;
        m_volumeScale = volumeScale;
    }
}
