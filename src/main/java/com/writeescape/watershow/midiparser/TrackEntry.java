package com.writeescape.watershow.midiparser;

import com.writeescape.watershow.instruments.Instrument;
import com.writeescape.watershow.instruments.InstrumentEntry;
import com.writeescape.watershow.utils.InOutParam;
import org.bukkit.block.data.BlockData;

/**
 *
 * @author SBPrime
 */
class TrackEntry {

    private long m_millis;

    private final NoteEntry m_note;

    public NoteEntry getNote() {
        return m_note;
    }

    public long getMillis() {
        return m_millis;
    }

    public void setMillis(long milis) {
        m_millis = milis;
    }

    public NoteEntry getEntry() {
        return m_note;
    }

    public TrackEntry(long millis, InstrumentEntry instrument, float volume) {
        float scale;

        final BlockData instrumentPatch;

        if (instrument != null) {
            scale = Math.max(0, instrument.getVolumeScale());
            instrumentPatch = instrument.getPatch();
        } else {
            scale = 0.0f;
            instrumentPatch = null;
        }

        m_millis = millis;
        final float vv = Math.max(0, Math.min(1, volume * scale)) * 3.0f;

        m_note = new NoteEntry(instrumentPatch, 12,8, vv);
    }

    public TrackEntry(long millis, Instrument instrument, int octave, int note, float volume) {
        float scale;

        InstrumentEntry iEntry;
        if (instrument == null) {
            iEntry = null;
        } else {
            InOutParam<Integer> startOctave = InOutParam.Out();
            iEntry = instrument.getEntry(octave, startOctave);
            if (iEntry != null && startOctave.isSet()) {
                octave -= startOctave.getValue();
            }
        }

        final BlockData instrumentPatch;

        if (iEntry != null) {
            scale = Math.max(0, iEntry.getVolumeScale());
            instrumentPatch = iEntry.getPatch();
        } else {
            scale = 0.0f;
            instrumentPatch = null;
        }

        m_millis = millis;


        final float vv = Math.max(0, Math.min(1, volume * scale)) * 3.0f;

        m_note = new NoteEntry(instrumentPatch, note,octave, vv);
    }

    @Override
    public int hashCode() {
        return (m_note != null ? m_note.hashCode() : 0) ^
                ((Long)m_millis).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        TrackEntry other = obj instanceof TrackEntry ? (TrackEntry)obj : null;
        if (other == null)
        {
            return false;
        }

        return m_millis == other.m_millis &&
                ((m_note == null && other.m_note == null) ||
                        (m_note!=null && m_note.equals(other.m_note)));
    }


}
