package com.writeescape.watershow.midiparser;

import com.writeescape.watershow.Watershow;
import com.writeescape.watershow.events.ShowEvents;
import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.util.Vector;
import org.bukkit.entity.Player;
import com.writeescape.watershow.configuration.ConfigProvider;

import java.util.logging.Level;

import static com.writeescape.watershow.Watershow.log;

import static org.bukkit.Material.TNT;
import static org.bukkit.Material.WHITE_STAINED_GLASS;

/**
 * MIDI note.
 *
 * @author SBPrime
 */
public class NoteEntry {

    private final BlockData m_instrumentPatch;
    private int note;
    private int octave;
    private final float m_volume;


    NoteEntry(BlockData instrumentPatch, int n, int o, float volume) {
        m_instrumentPatch = instrumentPatch;
        note = n;
        octave = o;
        m_volume = volume;
    }

    public void play(Location location, BaseShow s) {

        if(s != null){

            if(location.getWorld() != null) {
                Entity e = s.getEntity(note,octave,m_volume,m_instrumentPatch);
                ShowEvents showEvents = new ShowEvents(s);
                showEvents.setEntity(e);
                Bukkit.getServer().getPluginManager().callEvent(showEvents);
            }
        } else {
            log(Level.INFO,"BaseShow is Null");
        }



    }

    @Override
    public int hashCode() {
        return ((Integer) note).hashCode()
                ^ ((Integer) octave).hashCode()
                ^ ((Float) m_volume).hashCode()
                ^ (m_instrumentPatch != null ? m_instrumentPatch.hashCode() : 0);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NoteEntry) {
            NoteEntry other = (NoteEntry) obj;

            return note == other.note &&
                    octave == other.octave &&
                    //m_volume == other.m_volume &&
                    ((m_instrumentPatch == null && other.m_instrumentPatch == null) ||
                            (m_instrumentPatch != null && m_instrumentPatch.equals(other.m_instrumentPatch)));
        }

        return false;
    }


}
