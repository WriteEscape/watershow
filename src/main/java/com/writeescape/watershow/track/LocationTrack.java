package com.writeescape.watershow.track;
import com.writeescape.watershow.shows.BaseShow;
import com.writeescape.watershow.midiparser.NoteFrame;

import java.util.logging.Level;

import static com.writeescape.watershow.Watershow.log;
/**
 * This is a global player track.
 * Music played by this track is heard from specified location
 * for selected players
 * @author SBPrime
 */
public class LocationTrack extends BasePlayerTrack {


    public LocationTrack(NoteFrame[] notes, boolean loop, BaseShow show) {
        super(notes,loop,show);

    }

}
