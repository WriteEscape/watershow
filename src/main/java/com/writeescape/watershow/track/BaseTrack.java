package com.writeescape.watershow.track;
import com.writeescape.watershow.Watershow;
import com.writeescape.watershow.events.RotateShow;
import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import com.writeescape.watershow.configuration.ConfigProvider;
import com.writeescape.watershow.midiparser.NoteFrame;
import static com.writeescape.watershow.Watershow.log;
import java.util.Collection;
import java.util.logging.Level;

/**
 * Basic music track for playing notes
 * @author prime
 */
public abstract class BaseTrack {

    /**
     * Legth of 1/2 tick in miliseconds
     */
    private final static int HALF_TICK = 1000 / ConfigProvider.TICKS_PER_SECOND / 2;

    /**
     * Number of miliseconds to wait before performing loop
     */
    private final static int LOOP_WAIT = 1000;

    /**
     * Music track notes
     */
    private final NoteFrame[] m_notes;

    /**
     * Current track wait time
     */
    private long m_wait;

    /**
     * Is the track looped
     */
    private final boolean m_isLooped;

    /**
     * Track position
     */
    private int m_pos;

    /**
     * Next note to play
     */
    private NoteFrame m_nextNote;

    /**
     * Show linked to Track;
     */
    private final BaseShow show;
    /**
     * Use the per player sound location
     */
    protected BaseTrack(NoteFrame[] notes, boolean loop, BaseShow showname) {
        m_isLooped = loop;
        m_notes = notes;
        show = showname;
        log(Level.INFO,show.getName());
        show.setPlayingSong(true);
        rewind();
    }


    /**
     * Rewind track to the begining.
     * Allows you to add the track once again to the player.
     */
    public final void rewind() {
        m_pos = 0;
        if (m_notes != null && m_notes.length > 0) {
            m_nextNote = m_notes[0];
            m_wait = m_nextNote.getWait();
        } else {
            m_nextNote = null;
            m_wait = 0;
        }
    }

    /**
     * Get list of players that should hear the music
     *
     * @return The list of players
     */




    public void play(long delta) {
        m_wait -= delta;


        final Location location = show.getLocation();

        while (m_wait <= HALF_TICK && m_nextNote != null) {
            m_nextNote.play(location,show);
            m_pos++;

            if (m_pos < m_notes.length) {
                m_nextNote = m_notes[m_pos];
                m_wait += m_nextNote.getWait();
            } else if (m_isLooped) {
                m_pos %= m_notes.length;
                m_nextNote = m_notes[m_pos];

                m_wait += LOOP_WAIT;
            } else {
                m_nextNote = null;
            }

        }
    }

    /**
     * Is track finished
     *
     * @return Whether the track has finished playing or not
     */
    public boolean isFinished() {
        return m_nextNote == null;
    }
}
