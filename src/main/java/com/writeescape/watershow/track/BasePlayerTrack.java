package com.writeescape.watershow.track;

import com.writeescape.watershow.shows.BaseShow;
import org.bukkit.entity.Player;
import com.writeescape.watershow.midiparser.NoteFrame;

import java.util.logging.Level;

import static com.writeescape.watershow.Watershow.log;

/**
 * This is a basic track that holds a player list
 * @author SBPrime
 */
public abstract class BasePlayerTrack extends BaseTrack {
    public BasePlayerTrack(NoteFrame[] notes, boolean loop, BaseShow sname) {
        super(notes, loop, sname);

    }
}
