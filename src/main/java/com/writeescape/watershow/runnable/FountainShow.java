package com.writeescape.watershow.runnable;
import com.writeescape.watershow.Watershow;
import com.writeescape.watershow.managers.BlockCollector;
import com.writeescape.watershow.shows.BaseShow;
import com.writeescape.watershow.shows.Shows;
import com.writeescape.watershow.track.BaseTrack;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

/**
 * Main player timer class
 * @author SBPrime
 */
public class FountainShow implements Runnable {
    /**
     * Is the player running
     */
    private boolean m_isRunning;
    private boolean m_showRunning;
    /**
     * Last run enter time
     */
    private long m_lastEnter;

    /**
     * List of all playing music tracks
     */
    private final List<BaseTrack> m_playingTracks;
    private final List<BaseShow> m_playingShows;
    /**
     * The task
     */
    private final BukkitTask m_task;

    public FountainShow(Watershow plugin, BukkitScheduler scheduler) {
        m_lastEnter = System.currentTimeMillis();
        m_task = scheduler.runTaskTimer(plugin, this, 1, 1);
        m_playingTracks = new ArrayList<>();
        m_playingShows = new ArrayList<>();
        m_isRunning = true;
        m_showRunning = true;
    }

    /**
     * Stop the player
     */
    public void stop() {
        synchronized (m_playingTracks){
            m_isRunning = false;
            m_playingTracks.clear();

        }
        synchronized (m_playingShows){
            m_showRunning = false;
            m_playingShows.clear();
        }

        m_task.cancel();
    }

    @Override
    public void run() {
        final long now = System.currentTimeMillis();
        final long delta = now - m_lastEnter;
        m_lastEnter = now;

        final BaseTrack[] tracks;
        final BaseShow[] shows;
        synchronized (m_playingTracks) {
            tracks = m_playingTracks.toArray(new BaseTrack[0]);
        }
        synchronized (m_playingShows){
            shows = m_playingShows.toArray(new BaseShow[0]);
        }

        for (BaseTrack track : tracks) {

            track.play(delta);
            if (track.isFinished()) {

                removeTrack(track);
            }
        }
        for(BaseShow show : shows){
            show.play(delta);
            if(show.isFinished()){
                removeShow(show);
            }
        }

    }

    public void removeShow(BaseShow show){
        if(show == null){
            return;
        }
        synchronized (m_playingShows){
            m_playingShows.remove(show);
            show.remove();
        }
    }
    public void removeTrack(BaseTrack track) {
        if (track == null) {
            return;
        }
        synchronized (m_playingTracks) {
            m_playingTracks.remove(track);
        }
    }


    public void playShow(BaseShow show){
        if(show == null){
            return;
        }

        synchronized (m_playingShows){
            if(m_showRunning){
                m_playingShows.add(show);
            }
        }
    }
    public void playTrack(BaseTrack track) {
        if (track == null) {
            return;
        }

        synchronized (m_playingTracks) {
            if (m_isRunning) {
                m_playingTracks.add(track);
            }
        }
    }
    public List<BaseShow> getPlayingShows(){
            return m_playingShows;
    }
}